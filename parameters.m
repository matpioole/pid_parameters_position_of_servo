to = ScopeData.time;
uo = ScopeData.signals(1).values;
yo = ScopeData.signals(2).values;

t=to;
tau = 1.44;
[Lp, Mp]=pade(tau,8);
L=Lp;
M=conv(Mp,[2.04 0]);
y=lsim(L,M,uo,t);

plot(to,uo,to,yo,t,y);grid
legend('sterowanie','obiekt','aproksymacja');

% Nastawy regulatora obliczone na podstawie materialow pomocniczych:

%P = kp = (4/3)*(Tc/T)=1.9
%I = kp/Ti
%D = kp*Td

%Ti = 3*T
%Td = 0.42*T
%tr=4*T
